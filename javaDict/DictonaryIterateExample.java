//package com.adeepdrive.dictionary;

import java.util.Dictionary;
import java.util.Enumeration;
import java.util.Hashtable;

public class DictonaryIterateExample {
    public static void main(String[] args) {
        Dictionary<String, String> countryCurrency = new Hashtable<>();
        countryCurrency.put("USA", "USD");
        countryCurrency.put("Austria", "EUR");
        countryCurrency.put("Australia", "AUD");
        countryCurrency.put("France", "EUR");
        countryCurrency.put("Test ja", "test");
		
        Enumeration<String> keys = countryCurrency.keys();

        while (keys.hasMoreElements()) {
            String key = keys.nextElement();
            if (key.startsWith("Aus")) {
                String value = countryCurrency.get(key);
               System.out.println("Key : " + key + ", Value : " + value);
           }
        }

    }

}